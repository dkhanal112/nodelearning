const { response } = require('express');
const Joi = require('joi');
const express = require('express');
const app = express();
app.use(express.json());

const courses = [
    {id: 1, name: 'course1'},
    {id: 2, name: 'course2'},
    {id: 3, name: 'course3'},
]

app.get('/', (req, res) => {
    res.send('hello world')
});
app.get('/api/courses', (req, res) => {
    res.send([1, 2, 3, 4, 5]);
});

app.get('/api/courses/:year/:month', (req, res) => {
    res.send(req.query);
});


//Hnadling post req
app.post('/api/courses', (req, res) => {
const schema = {
    name: Joi.string
}
    if(!req.body.name || req.body.name.length<3){
        response.status(400).send("Name is required and should be minimum 3 characters");
        return;
    }
    const course = {
        id: courses.length + 1,
        name: req.body.name,
    };
    courses.push(course);
    res.send(course);


});

//PORT

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`listening to the port ${port}`);
});



